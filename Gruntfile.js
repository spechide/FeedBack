module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        htmlhint: {
            options: {
                'attr-lower-case': true,
                'attr-value-not-empty': false,
                'tag-pair': true,
                'tag-self-close': false,
                'tagname-lowercase': true,
                'id-class-value': true,
                'id-unique': true,
                'img-alt-require': true,
                'img-alt-not-empty': true
            },
            main: {
                src: ['public/**.html']
            }
        },

        jshint: {
            main: {
                files: [{
                    src: ['Gruntfile.js', 'js/**.js']
                }]
            },
            options: {
                globals: {
                    'jQuery': true,
                    'console': true
                }
            }
        },

        copy: {
            fonts: {
                expand: true,
                files: [{
                    expand: true,
                    cwd: 'bower_components/fonts-smc/fonts',
                    src: "**",
                    dest: 'public/fonts'
                }]
            },
            iejs: {
                expand: true,
                files: [{
                        expand: true,
                        cwd: 'bower_components/html5shiv/dist',
                        src: "**",
                        dest: 'public/js'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/respond/src',
                        src: "**",
                        dest: 'public/js'
                    }
                ]
            }
        },

        pug: {
            options: {
                pretty: true,
                data: {
                    debug: true
                }
            },
            pages: {
                files: [{
                    expand: true,
                    cwd: 'pug/pages',
                    src: '*.pug',
                    dest: 'public/',
                    ext: '.html'
                }]
            }
        },

        uglify: {
            compress: {
                files: {
                    'public/js/index.js': [
                        'js/*.js'
                    ],
                    'public/js/vendor.js': [
                        'bower_components/jquery/dist/jquery.min.js',
                        'bower_components/bootstrap/dist/js/bootstrap.min.js'
                    ]
                },
                options: {
                    mangle: true,
                    unused: true
                }
            }
        },

        cssmin: {
            options: {
                sourceMap: true
            },
            compress: {
                files: {
                    'public/css/index.css': [
                        'css/*.css'
                    ],
                    'public/css/vendor.css': [
                        'bower_components/smc-fonts/css/fonts.css',
                        'bower_components/bootstrap/dist/css/bootstrap.min.css',
                        'bower_components/font-awesome/css/font-awesome.min.css',
                        'bower_components/normalize-css/normalize.css'
                    ]
                }
            }
        },

        watch: {
            grunt: {
                files: ['Gruntfile.js'],
                options: {
                    nospawn: true,
                    keepalive: true,
                    livereload: true
                },
                tasks: ['public:dev']
            },
            html: {
                files: ['public/*.html'],
                options: {
                    nospawn: true,
                    keepalive: true,
                    livereload: true
                },
                tasks: ['htmlhint']
            },
            js: {
                files: 'js/**',
                options: {
                    nospawn: true,
                    livereload: true
                },
                tasks: ['jshint', 'uglify']
            },
            pug: {
                files: ['pug/**'],
                options: {
                    nospawn: true,
                    livereload: true
                },
                tasks: ['pug']
            },
            css: {
                files: 'css/**',
                options: {
                    nospawn: true,
                    livereload: true
                },
                tasks: ['cssmin']
            }
        },

        connect: {
            server: {
                options: {
                    port: 8080,
                    base: 'public',
                    hostname: 'localhost',
                    livereload: true
                }
            }
        }
    });

    grunt.registerTask('public:dev', function(target) {
        grunt.task.run([
            'htmlhint',
            'jshint',
            'pug',
            'copy',
            'uglify',
            'cssmin'
        ]);
    });

    grunt.registerTask('serve', function(target) {
        grunt.task.run([
            'public:dev',
            'connect',
            'watch'
        ]);
    });

};
