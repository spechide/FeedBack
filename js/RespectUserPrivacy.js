var trackOff = function() {
    if (document.getElementById('cookiewarning')) {
        document.getElementById('cookiewarning').style.display = 'none';
    }
};
var trackOn = function() {
    if (document.getElementById('cookiewarning')) {
        document.getElementById('cookiewarning').style.display = 'block';
    }
};
// respect user's privacy
setInterval(function() {
    if (navigator.doNotTrack != "yes" && navigator.doNotTrack != "1" && navigator.msDoNotTrack != "1") {
        // analytics_and_adsense();
        // => analytics and adsense code HERE
        trackOn();
    } else {
        // => please do not ue the tracking and adesnse codes here
        trackOff();
    }
}, 1000);
