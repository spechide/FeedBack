$(window).bind('keydown', function(event) {
    // http://stackoverflow.com/questions/93695/best-cross-browser-method-to-capture-ctrls-with-jquery
    if (event.ctrlKey || event.metaKey) {
        switch (String.fromCharCode(event.which).toLowerCase()) {
            case 's':
                event.preventDefault();
                DisplayMessageInModal(
                    "Please do not plagiarize this work!",
                    "You may take the source code of this website from <a href='https://gitlab.com/spechide/spechide.gitlab.io'>here</a>."
                );
                break;
            case 'a':
                event.preventDefault();
                DisplayMessageInModal(
                    "What is this?",
                    "This website is an attempt to make my online presence stronger! The idea is inspired from <a href='http://webkay.robinlinus.com/'>What every Browser knows about you</a>."
                );
                break;
            case 'b':
            case 'c':
            case 'f':
                event.preventDefault();
                DisplayMessageInModal(
                    "SpEcHiDe is here to help you!",
                    "Use the 'LEFT ARROW' or the 'RIGHT ARROW' to navigate between the two pages, at any time!"
                );
                break;
        }
    } else {
        switch (event.which) {
            // => http://stackoverflow.com/a/5597114/4723940
            case 37:
                event.preventDefault();
                // LEFT arrow key
                document.getElementById('Anm0usFB').click();
                // console.log("LEFT: horizontal scrolling disabled");
                break;
            case 38:
                //event.preventDefault();
                //alert("up");
                break;
            case 39:
                event.preventDefault();
                // RIGHT arrow key
                document.getElementById('FillMeriSB').click();
                // console.log("RIGHT: horizontal scrolling disabled");
                break;
            case 40:
                //event.preventDefault();
                //alert("down");
                break;
                // => http://stackoverflow.com/a/6011119/4723940
        }
    }
    // http://stackoverflow.com/questions/93695/best-cross-browser-method-to-capture-ctrls-with-jquery
});

var doSubmitSB = function() {
    var formElement = document.querySelector("#slamBookForm");
    var formData = new FormData(formElement);
    formData.append('putquestions', 'snoitseuqtup');
    RequestData("POST", "//spechide.shrimadhavuk.me/functions/merislambook.php" + "?d=0", formData, function(u, r) {
        if (r == "t") {
            document.getElementById("slamBookForm").innerHTML = "<h1>Shrimadhav U K thanks you for your time</h1>";
        } else {
            errorMessage(u, r);
        }
    });
};

var doSubmitFB = function() {
    var formElement = document.querySelector("#feedBackForm");
    var formData = new FormData(formElement);
    formData.append('putquestions', 'snoitseuqtup');
    RequestData("POST", "//spechide.shrimadhavuk.me/functions/feedback.php" + "?d=0", formData, function(u, r) {
        if (r == "t") {
            document.getElementById("feedBackForm").innerHTML = "<h1>Shrimadhav U K thanks you for your time</h1>";
        } else {
            errorMessage(u, r);
        }
    });
};

var getQuestions = function() {
    var formdata = new FormData(); // Currently empty
    formdata.append('getquestions', 'snoitseuqteg');
    RequestData("POST", "//spechide.shrimadhavuk.me/functions/merislambook.php" + "?d=0", formdata, function(u, r) {
        if (r.indexOf("ERROR") == -1) {
            document.getElementById("useless_questions").innerHTML = r;
        } else {
            errorMessage(u, r);
        }
    });
};

function geocodeAddress(geocoder, resultsMap) {
    var address = document.getElementById("addr_pinno").value;
    geocoder.geocode({
        "address": address
    }, function(results, status) {
        if (status === "OK") {
            console.log(results.length);
            var stluser = results[0];
            var acmpnts = stluser.address_components;
            var lcn = stluser.geometry.location;
            document.getElementById("addr_town").value = acmpnts[1].long_name;
            document.getElementById("addr_state").value = acmpnts[2].long_name;
            document.getElementById("addr_country").value = acmpnts[3].long_name;
            document.getElementById("latitude").value = lcn.lat();
            document.getElementById("longitude").value = lcn.lng();
        } else {
            console.warn("Geocode was not successful for the following reason: " + status);
        }
    });
}
